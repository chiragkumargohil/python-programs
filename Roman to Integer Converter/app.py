'''

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.

'''

def roman_to_int(user_input):
    roman_numbers_dictionary = {
        'I' : 1,
        'V' : 5,
        'X' : 10,
        'L' : 50,
        'C' : 100,
        'D' : 500,
        'M' : 1000,
        'IV' : 4,
        'IX' : 9,
        'XL' : 40,
        'XC' : 90,
        'CD' : 400,
        'CM' : 900
    }
	
    num = 0
    i = 0
    while i < len(user_input):
        letter = user_input[i]
        consecutive_letters = user_input[i:i+2]

        if letter in roman_numbers_dictionary:
            if i + 1 < len(user_input) and consecutive_letters in roman_numbers_dictionary:
                num += roman_numbers_dictionary[consecutive_letters]
                i += 2
            else:
                num += roman_numbers_dictionary[letter]
                i += 1
        else:
            return "Invalid Roman Number."
    
    return num
