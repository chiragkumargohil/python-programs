# Print Fibonacci Series upto Given Term

import time

def fibonacci(upTo):
    a = 0
    b = 1
    x = 0 # this variable is used to assign variable 'b' to varible 'a' for consecutive iterations

    fib_li = []
    for i in range(upTo):
        fib_li.append(a)
        x = b
        b = a + b
        a = x
    return fib_li

user = int(input("Upto ___ term : "))
a = fibonacci(user)
print(f"\nList of first {user} term(s) is : {a}") # this prints list of Fibonacci Series upto given number
print(f"Term {user} : {a[user-1]}") # this prints nth number

time.sleep(5) # this holds your program for 5 seconds before exiting the program
