# Prime Numbers Problem : This program return list of First n Prime Numbers or list of Prime Numbers from 1 to given number or Check whether given number is prime or not

import time

class PrimeNumbers:

    def __init__(self, inp):
        self.user_input = inp
    
    def primeNumbers(self):
        li = []
        count = 0
        i = 2
        while count != self.user_input:
            re = True
            for n in range(2, i):
                if (i % n == 0):
                    re = False
            if (re):
                li.append(i)
                count += 1
            i += 1
        print(f"\nList of First {self.user_input} Prime Numbers : {li}")

    def primeNumbersUpto(self):
        li = []
        for i in range(2, (self.user_input+1)):
            re = True
            for n in range(2, i):
                if (i % n == 0):
                    re = False
            if (re):
                li.append(i)
        print(f"List of Prime Numbers from first {self.user_input} Natural Numbers : {li}")

    def primeCheck(self):
        re = True
        if self.user_input == 1:
            print("'1' may or may not be a Prime Number.")
        else:
            for n in range(2, self.user_input):
                if (self.user_input % n == 0):
                    re = False
                    break
            if (re):
                print(f"Yes. Given Number, {self.user_input} is a Prime Number.")
            else:
                print(f"No. Given Number, {self.user_input} is not a Prime Number.")

msg = '''
Type '1' for printing the First _ Prime Numbers
Type '2' for printing Prime Numbers upto Your response
Type '3' for checking whether the given number is Prime Number or Not
Type '4' to Quit
'''

while True:
    print(msg)
    try:
        user = int(input("What do You want to do? Select from above options. "))
        if user == 1 or user == 2 or user == 3:
            pass
        elif user == 4:
            print("Closing...")
            break
        else:
            print("You are supoosed to type either '1' or '2' or '3' or '4'.")
    except:
        print("\nEnter valid response from menu.")
        continue
    
    ex = int(input("Enter Number : "))
    ans = PrimeNumbers(ex)

    if user == 1:
            ans.primeNumbers()
    elif user == 2:
        ans.primeNumbersUpto()
    elif user == 3:
        ans.primeCheck()
    
time.sleep(3)
