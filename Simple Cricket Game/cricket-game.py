# Simple cricket game : A random result (0, 1, 2, 3, 4, 6, W)  will be generated when you hit by typing '1'.

import random, time

possibility = ["0", "1", "2", "3", "4", "6", "W"]

score = 0
wickets = 0
hit = None

while wickets != 10:
    hit = input("Hit by typing '1' OR quit by typing'0' : ")
    if hit == '1':
        pass
    elif hit == "0":
        print(f"\nScorecard : {score}/{wickets}")
        print("See you again...")
        time.sleep(3)
        exit()
    else:
        print("\nType either '1' or '0'...")
        continue
        
    run = random.choice(possibility)
    if run in ["0", "1", "2", "3", "4", "6"]:
        score += int(run)
        print(f"Run on this ball {run}")
    elif run == "W":
        wickets += 1
        print("It's wicket!")
    print(f"Scorecard : {score}/{wickets}")
    
print(f"\nTeam All Out!\nFinal Socre is {score}/{wickets}\nClosing...")
time.sleep(3)
