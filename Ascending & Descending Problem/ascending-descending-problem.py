# Ascending or Descending Order of Given List

import time

class AscedingDescendingOrder:

    def __init__(self, li):
        self.list = li
        print("\nYour List in Ascending / Descending Order...\n")

    # 1st Method for Ascending (sort) or Descending (reverse) Order
    def sortingMethod(self):
        print("This method will print both, Ascending and Descending Order of given list respectively...")
        self.list.sort()
        print(self.list) # this will print a list in Descending Order
        self.list.reverse()
        print(self.list) # this will print a list in Descending Order

    # 2nd Method for for Ascending (<) or Descending (>) Order
    def method_1(self):
        for i in range(len(self.list)):
            m = self.list[0]
            for j in range(i, len(self.list)):
                if self.list[j] < self.list[i]: # Change "<" to ">" for Descending Order
                    m = self.list[i]
                    self.list[i] = self.list[j]
                    self.list[j] = m
        print(self.list)

    # 3rd Method for Ascending (<) or Descending (>) Order
    def method_2(self):    
        a = []
        for i in range(len(self.list)):
            mV = self.list[0]
            for j in range(len(self.list)): # Change "<" to ">" for Descending Order
                if self.list[j] < mV:
                    mV = self.list[j]
            a.append(mV)
            self.list.remove(mV)
        self.list = a
        print(self.list)

# Example
li = [2, 3, 1, 9, 0, 5, 1, 4, 6, 8, 7]
print(f"Example : {li}") # this will print the given list
ans = AscedingDescendingOrder(li)
ans.method_1()  # this will print ordered list

time.sleep(5) # this will hold your program for 5 seconds
